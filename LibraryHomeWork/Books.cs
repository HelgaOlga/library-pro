﻿using System;
namespace LibraryHomeWork
{
    public class Books
    {
        public string Name;
        public string Author;

        /// <summary>
        /// инициализируем поля при создании класса
        /// </summary>
        /// <param name="name"></param>
        /// <param name="author"></param>
        public Books(string name, string author)
        //конструктор для книги
        {

            Name = name;
            Author = author;

        }

    }
}
